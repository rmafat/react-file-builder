/**
 * Uppercase the first letter of word and, let the rest to lowercase.
 *
 * @param word
 */
export const initCap = (word: string) => word.charAt(0).toUpperCase() + word.slice(1);

/**
 * Check if obj has all given props has key.
 *
 * @param obj
 * @param props
 */
export const objectHasStructure = (obj: any, props: string[]): boolean => (
  props.find((prop) => obj[prop] === undefined) === undefined
);

/**
 * Add a trailing slash to strings that don't have one.
 *
 * @param value
 */
export const addTrailingSlash = (value: string): string => {
  const lastCharacter = value.substr(-1);

  return '/' === lastCharacter ? value : `${value}/`;
};
