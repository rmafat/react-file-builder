import { exec } from 'child_process';
import { red } from 'chalk';

/**
 * Execute an instruction on command line interface.
 *
 * @param command
 */
export const commandLine = (command: string): Promise<string> => (
  new Promise((resolve) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.log(red(`error: ${error.message}`));
        return;
      }
      if (stderr) {
        console.log(red(`stderr: ${stderr}`));
      }

      // console.log(stdout);
      resolve(stdout);
    });
  })
);
