import { TemplateParams } from '../models/Templates';

/**
 * Transform an object on string to be usable as Hygen template params.
 *
 * @param params
 */
export const formatTemplateParam = (params: TemplateParams): string => (
  Object.keys(params).reduce((acc: string, paramKey: string) => (
    // @ts-ignore
    `${acc} --${paramKey} ${params[paramKey]}`
  ), '')
);
