import * as path from 'path';
import fs, { Dirent } from 'fs';

/**
 * Get the absolute path of an element, from the CLI directory.
 *
 * @param filePath
 *   The path we want from the CLI directory.
 */
export const fromRfbDir = (filePath: string): string => {
  if (!require.main) {
    throw new Error('An error occurred while creating Concerto structure.');
  }

  const distDir = path.dirname(require.main.filename);
  const distDirSplitted = distDir.split('/');

  // Remove dist directory.
  distDirSplitted.pop();

  return `${distDirSplitted.join('/')}${filePath}`;
};

/**
 * Get the current working directory.
 *
 * @param filePath
 */
export const fromCwd = (filePath: string): string => {
  const formattedFilePath = filePath.substr(0, 1) === '/'
    ? filePath.substr(1)
    : filePath;

  return `${process.cwd()}/${formattedFilePath}`;
};

/**
 * Parse Json object from a given Json file.
 *
 * @param filePath
 */
export const parseJsonFile = (filePath: string): any => {
  if (fs.existsSync(filePath)) {
    return JSON.parse(fs.readFileSync(filePath, 'utf8'));
  }

  throw new Error(`The file ${filePath} doesn't exists.`);
};

/**
 * List files inside a directory.
 *
 * @param directory
 */
export const logDirectoryFiles = (directory: string): any => {
  if (!fs.existsSync(directory)) {
    throw new Error(`The directory ${directory} doesn't exists.`);
  }

  const files: Dirent[] = fs.readdirSync(directory, {
    encoding: 'utf8',
    withFileTypes: true,
  });

  files.map((file: Dirent) => console.log(`* ${directory}/${file.name}`));
};
