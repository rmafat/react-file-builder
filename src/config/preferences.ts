import { Preferences, PreferencesKeys, StylesExtensions } from '../models/Preferences';

export const RFB_CONFIG_FILE = '.rfb.json';

export const preferencesDefaultValues: Preferences = {
  [PreferencesKeys.REACT_NATIVE]: false,
  [PreferencesKeys.TYPESCRIPT]: false,
  [PreferencesKeys.TESTS]: true,
  [PreferencesKeys.STYLES]: true,
  [PreferencesKeys.STYLES_EXTENSION]: StylesExtensions.SCSS,
  [PreferencesKeys.DIRECTORY]: 'src/components/',
};
