import { red } from 'chalk';
import { addTrailingSlash, initCap } from '../helpers/generic';
import { readPreferences } from '../handlers/preferencesHandler';
import { generateComponentFiles } from '../handlers/fileGenerator';
import { checkForExistingComponent } from '../handlers/fileChecker';
import { PreferencesKeys } from '../models/Preferences';
import { Options } from '../models/Command';

/**
 * Command controller to generate React components.
 *
 * @param name
 * @param options
 */
export const generateComponent = async (name: string, options: Options): Promise<void> => {
  try {
    const preferences = readPreferences();
    const componentName = initCap(name);

    if (options.dir) {
      preferences[PreferencesKeys.DIRECTORY] = addTrailingSlash(options.dir);
    }

    await checkForExistingComponent(preferences[PreferencesKeys.DIRECTORY] + componentName);
    await generateComponentFiles(componentName, preferences);
  } catch (e) {
    console.log(red(e));
  }
};
