import inquirer from 'inquirer';
import { green } from 'chalk';
import { Preferences, PreferencesKeys, StylesExtensions } from '../models/Preferences';
import { generateRfbPreferencesFile } from '../handlers/fileGenerator';
import { preferencesDefaultValues } from '../config/preferences';

/**
 * Prompt to ask user preferences.
 */
const askForPreferences = (): Promise<Preferences> => (
  inquirer.prompt([{
    type: 'confirm',
    name: PreferencesKeys.REACT_NATIVE,
    message: 'Is it a React Native project ?',
    default: preferencesDefaultValues[PreferencesKeys.REACT_NATIVE],
  }, {
    type: 'confirm',
    name: PreferencesKeys.TYPESCRIPT,
    message: 'Do you use Typescript on your project ?',
    default: preferencesDefaultValues[PreferencesKeys.TYPESCRIPT],
  }, {
    type: 'confirm',
    name: PreferencesKeys.TESTS,
    message: 'Do you want RFB command to generate tests files for each component ?',
    default: preferencesDefaultValues[PreferencesKeys.TESTS],
  }, {
    type: 'confirm',
    name: PreferencesKeys.STYLES,
    message: 'Do you want RFB command to generate styles files for each components ?',
    default: preferencesDefaultValues[PreferencesKeys.STYLES],
  }, {
    type: 'list',
    name: PreferencesKeys.STYLES_EXTENSION,
    message: 'What styles extension do you use ?',
    choices: Object.values(StylesExtensions),
    default: preferencesDefaultValues[PreferencesKeys.STYLES_EXTENSION],
    when: (answers): boolean => answers.generateStyles,
  }, {
    type: 'input',
    name: PreferencesKeys.DIRECTORY,
    message: 'Where do you want to generate your components ?',
    default: preferencesDefaultValues[PreferencesKeys.DIRECTORY],
  }])
);

/**
 * Command controller to initialize RFB by defining user preferences.
 */
export const init = async () => {
  const userPreferences: Preferences = await askForPreferences();
  // To be sure preferences are complete, for example if styles extensions are skipped.
  const completePreferences: Preferences = {
    ...preferencesDefaultValues,
    ...userPreferences,
  };
  generateRfbPreferencesFile(completePreferences);

  console.log(green('\n.rfb file generated successfully !'));
};
