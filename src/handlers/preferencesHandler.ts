import { yellow } from 'chalk';
import { Preferences, PreferencesKeys, StylesExtensions } from '../models/Preferences';
import { fromCwd, parseJsonFile } from '../helpers/fileSystem';
import { addTrailingSlash, objectHasStructure } from '../helpers/generic';
import { preferencesDefaultValues, RFB_CONFIG_FILE } from '../config/preferences';

/**
 * Apply some check to user preferences.
 *
 * @param preferences
 */
const checkPreferences = (preferences: Preferences): Preferences => {
  const checkedPreferences = preferences;

  if (!Object.values(StylesExtensions).includes(preferences[PreferencesKeys.STYLES_EXTENSION])) {
    console.log(yellow(`
      .rfb file hasn\'t valid values for '${PreferencesKeys.STYLES_EXTENSION}' key.
      Allowed values are : ${Object.values(StylesExtensions).join(', ')}.
      Default value '${preferencesDefaultValues[PreferencesKeys.STYLES_EXTENSION]}' will be used.
    `));

    checkedPreferences[PreferencesKeys.STYLES_EXTENSION] = preferencesDefaultValues[PreferencesKeys.STYLES_EXTENSION];
  }

  if (!objectHasStructure(preferences, Object.values(PreferencesKeys))) {
    console.log(yellow(`
      .rfb file hasn\'t the complete required structure.
      Default values will be used.
      Please run 'rfb init' to regenerate it.
    `));
  }

  return checkedPreferences;
};

/**
 * Format some user preferences values to ensure validity.
 *
 * @param preferences
 */
const formatPreferences = (preferences: Preferences): Preferences => {
  const formattedPreferences = preferences;

  if (preferences[PreferencesKeys.DIRECTORY]) {
    formattedPreferences[PreferencesKeys.DIRECTORY] = addTrailingSlash(preferences[PreferencesKeys.DIRECTORY]);
  }

  return formattedPreferences;
};

/**
 * Read preferences from .rgb user file.
 */
export const readPreferences = (): Preferences => {
  let userPreferences: Preferences = parseJsonFile(fromCwd(RFB_CONFIG_FILE));

  userPreferences = formatPreferences(userPreferences);
  userPreferences = checkPreferences(userPreferences);

  return {
    ...preferencesDefaultValues,
    ...userPreferences,
  };
};
