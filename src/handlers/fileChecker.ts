import fs from 'fs';
import inquirer from 'inquirer';
import { yellow, red } from 'chalk';

interface OverrideQuestion {
  override: boolean;
}

/**
 * Ask the user for override the existing component.
 */
const askForOverride = (): Promise<OverrideQuestion> => (
  inquirer.prompt([{
    type: 'confirm',
    name: 'override',
    message: 'Do you want to override the component and reset it ?',
    default: false,
  }])
);

/**
 * Check if the component already exists and warn the user.
 *
 * @param componentPath
 */
export const checkForExistingComponent = async (componentPath: string): Promise<void> => {
  if (fs.existsSync(componentPath)) {
    console.log(yellow(`Warning : It seems that the ${componentPath} component already exists.`));
    const question: OverrideQuestion = await askForOverride();

    if (!question.override) {
      console.log(red('Command stopped, the component is not created.'));
      process.exit();
    }
  }
};
