import fs from 'fs';
import { red, green } from 'chalk';
import { Preferences, PreferencesKeys } from '../models/Preferences';
import { fromRfbDir, logDirectoryFiles } from '../helpers/fileSystem';
import { commandLine } from '../helpers/cli';
import { TemplateParams } from '../models/Templates';
import { formatTemplateParam } from '../helpers/fileGenaration';
import { RFB_CONFIG_FILE } from '../config/preferences';

/**
 * Write user preferences into .rfb conf file.
 */
export const generateRfbPreferencesFile = (preferences: Preferences): void => {
  fs.writeFile(
    RFB_CONFIG_FILE,
    JSON.stringify(preferences, null, 2),
    (error) => error && console.error(red(error)),
  );
};

/**
 * Generate a template using Hygen command.
 *
 * @param templateName
 *   The template name to generate.
 * @param params
 *   All params passed to template.
 */
const generateTemplate = async (templateName: string, params: TemplateParams): Promise<void> => {
  const templateDir = fromRfbDir('/_templates');
  const environment = `HYGEN_TMPLS=${templateDir} HYGEN_OVERWRITE=1`;
  const command = `${environment} hygen rfb ${templateName} ${formatTemplateParam(params)}`;

  await commandLine(command);
};

/**
 * Log list of files generated on the user console.
 *
 * @param params
 */
const logCreatedFiles = (params: TemplateParams): void => {
  console.log(green('Created files : \n'));
  logDirectoryFiles(params[PreferencesKeys.DIRECTORY] + params.componentName);
};

/**
 * Generate all components files, depending on user preferences.
 *
 * @param componentName
 * @param preferences
 */
export const generateComponentFiles = async (componentName: string, preferences: Preferences): Promise<void> => {
  const params: TemplateParams = {
    ...preferences,
    componentName,
  };

  await generateTemplate(preferences[PreferencesKeys.TYPESCRIPT] ? 'TSXComponent' : 'JSXComponent', params);

  if (preferences[PreferencesKeys.STYLES]) {
    await generateTemplate('styles', params);
  }
  if (preferences[PreferencesKeys.TESTS]) {
    await generateTemplate('tests', params);
  }

  logCreatedFiles(params);
};
