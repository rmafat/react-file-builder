import { Preferences } from './Preferences';

export interface TemplateParams extends Preferences {
  componentName: string;
}
