export enum StylesExtensions {
  CSS = '.css',
  SASS = '.sass',
  SCSS = '.scss',
  JS = '.js',
  TS = '.ts',
}

export enum PreferencesKeys {
  REACT_NATIVE = 'isReactNative',
  TYPESCRIPT = 'useTypescript',
  TESTS = 'generateTests',
  STYLES = 'generateStyles',
  STYLES_EXTENSION = 'stylesExtension',
  DIRECTORY = 'rootComponentsDirectory',
}

export interface Preferences {
  [PreferencesKeys.REACT_NATIVE]: boolean;
  [PreferencesKeys.TYPESCRIPT]: boolean;
  [PreferencesKeys.TESTS]: boolean;
  [PreferencesKeys.STYLES]: boolean;
  [PreferencesKeys.STYLES_EXTENSION]: StylesExtensions;
  [PreferencesKeys.DIRECTORY]: string;
}
