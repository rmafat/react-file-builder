# React File Builder

CLI tools to quickly generate React files.

1. [Quick Start](#quick-start)
2. [Installation](#installation)
3. [Initialization](#initialization)
4. [Basic usage](#basic-usage)
5. [Options](#options)
6. [Configuration](#configuration)
7. [Motivation](#motivation)


## Quick start

```bash
# Install package on your system
npm install -g react-file-builder

# Define preferences for your project
> rfb init

# Generate your first component files
> rfb component MyComponent

# Generate component files in a different folder than the default one
> rfb component MyComponent -d path/to/specific/directory
```

Look at the following documentation to alternative installation, 
more precisions on preferences, and options.

## Installation

You can install react-file-builder *globally* :

```bash
npm install -g react-file-builder
```

OR install it on your *project* only :

```bash
npm install --save-dev react-file-builder
```

**IMPORTANT** : In the second case, you have to add this to your `$PATH` to use the `rfb` command:

If you use **bash** :

```bash
echo 'export PATH="./node_modules/.bin:$PATH"' >> ~/.bashrc
```

If you use **zsh** :

```bash
echo 'export PATH="./node_modules/.bin:$PATH"' >> ~/.zshrc
```


## Initialization 

Initialize the `.rfb.json` file that contains the configuration.  
The command ask you some information about your project and your preferences.   
See the [configure](#configuration) part for more details.

```bash
> rfb init
```

## Basic usage

This command will generate the following file structure :

```bash
> rfb component Avatar 
```

```bash
- src/
--- components/
----- Avatar/
------- Avatar.tsx
------- Avatar.css
------- Avatar.test.tsx
------- index.ts
```

And `Avatar.tsx` contains : 

```tsx
import React from 'react';
import './Avatar.css';

interface Props {}

const Avatar: React.FC<Props> = (props: Props) => {
  return (
    <>
    </>
  );
}

export default Avatar;
```

Of course, it's possible to [configure](#configuration) this generation.

## Options
### --dir

Specify the directory where to generate component files.    
:information_source: The `rootComponentsDirectory` define in preferences is NOT used with this option.

If some specified directories doesn't exist, rfb will create them for you :wink:.

Usage : 
```bash
> rfb component MyComponent --dir path/to/specific/directory

# or shorter
> rfb component MyComponent -d path/to/specific/directory
```

The above commend will generate :
```bash
- path/
--- to/
----- specific/
------- directory/
--------- MyComponent/
----------- MyComponent.tsx
----------- MyComponent.css
----------- MyComponent.test.tsx
----------- MyComponent.ts
```

## Configuration

Run `rfb init` to generate a `.rfb.json` file with configuration.

```bash
{
  "isReactNative": false,
  "useTypescript": false,
  "generateTests": true,
  "generateStyles": true,
  "stylesExtension": ".scss",
  "rootComponentsDirectory": "src/components/"
}
```

| Property                    | Type                                           | Description                                                                                                                                                                                                                              |
| --------------------------- | ---------------------------------------------- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **isReactNative**           | Boolean                                        | Define if it's a React Native project.                                                                                                                                                                                                   |
| **useTypescript**           | Boolean                                        | Define if RFB generate a test file for your component.                                                                                                                                                                                   |
| **generateTests**           | Boolean                                        | Define if RGB generate a style file for your component.                                                                                                                                                                                  |
| **generateStyles**          | Boolean                                        | Define if RFB generate styles files                                                                                                                                                                                                      |
| **stylesExtension**         | One of [".css", ".sass",".scss", ".ts", ".js"] | Define the style extension for your style file.<br />**Note**: if you use `.ts` or `.js` file extension (for React Native project),  RFB will add "Styles" suffix on your style file, to avoid confusion with the component, for import. |
| **rootComponentsDirectory** | String (with trailing slash)                   | Define the default directory on which RFB will generate your component.                                                                                                                                                                  |



## Motivation

React folder structure is free, and each developer can make his own file structure.
But which is the best ? Is there a best solution ?

This module doesn't provide a solution, it only help people using this approach : **1 component = 1 folder with all component files**.

Structure example :

```bash
- public/
- src/
--- actions/			
--- components/
----- Button/
------- Button.jsx
------- Button.scss
------- Button.test.jsx
------- index.js
----- Profile/
------- Avatar/
--------- Avatar.jsx
--------- Avatar.scss
--------- Avatar.test.jsx
--------- index.js
------- UserDetails.jsx
--------- UserDetails.scss
--------- UserDetails.test.jsx
--------- index.js
--- config/			
--- contexts/			
--- helpers/			
--- hooks/
--- models/
--- reducers/
--- services/
```

Each developer is free to organize his components as he wants, and it can depend on project.

The important part is the component structure :

```bash
- Avatar/
--- Avatar.jsx
--- Avatar.scss
--- Avatar.test.jsx
--- index.js
```

Let's assume that each component is a new folder with the component itself, its style file, and its tests.
(The index.js allow avoiding `import from ./Avatar/Avatar`.)
This structure makes it possible to have a well-cut and easily maintainable application.

However, there is a problem that impacts productivity : creating this structure each time we need a new component.

This is what React File Builder is made for !   
RFB command line allow to easily generate this kind of structure very quickly.


This package will have some new features on the future. If you have any suggestions or ideas, please tell me !
