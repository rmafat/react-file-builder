#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const commander_1 = tslib_1.__importDefault(require("commander"));
const fileSystem_1 = require("./src/helpers/fileSystem");
const component_1 = require("./src/commands/component");
const init_1 = require("./src/commands/init");
// Register the tsconfig allow CLI to read TS files.
const tsConfig = fileSystem_1.parseJsonFile(fileSystem_1.fromRfbDir('/tsconfig.json'));
require('ts-node').register(tsConfig);
commander_1.default.version('1.0.0');
commander_1.default.command('init')
    .description('Initialize and configure react-file-builder command.')
    .action(() => init_1.init());
commander_1.default.command('component <componentName>')
    .description('Generate files for a JSXComponent JSXComponent.')
    .option('-d, --dir <directoryPath>', 'Directory path where to generate the component.')
    .action((componentName, options) => component_1.generateComponent(componentName, { dir: options.dir }));
commander_1.default.parse(process.argv);
