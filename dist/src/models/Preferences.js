"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PreferencesKeys = exports.StylesExtensions = void 0;
var StylesExtensions;
(function (StylesExtensions) {
    StylesExtensions["CSS"] = ".css";
    StylesExtensions["SASS"] = ".sass";
    StylesExtensions["SCSS"] = ".scss";
    StylesExtensions["JS"] = ".js";
    StylesExtensions["TS"] = ".ts";
})(StylesExtensions = exports.StylesExtensions || (exports.StylesExtensions = {}));
var PreferencesKeys;
(function (PreferencesKeys) {
    PreferencesKeys["REACT_NATIVE"] = "isReactNative";
    PreferencesKeys["TYPESCRIPT"] = "useTypescript";
    PreferencesKeys["TESTS"] = "generateTests";
    PreferencesKeys["STYLES"] = "generateStyles";
    PreferencesKeys["STYLES_EXTENSION"] = "stylesExtension";
    PreferencesKeys["DIRECTORY"] = "rootComponentsDirectory";
})(PreferencesKeys = exports.PreferencesKeys || (exports.PreferencesKeys = {}));
