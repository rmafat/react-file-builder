"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.commandLine = void 0;
const child_process_1 = require("child_process");
const chalk_1 = require("chalk");
/**
 * Execute an instruction on command line interface.
 *
 * @param command
 */
const commandLine = (command) => (new Promise((resolve) => {
    child_process_1.exec(command, (error, stdout, stderr) => {
        if (error) {
            console.log(chalk_1.red(`error: ${error.message}`));
            return;
        }
        if (stderr) {
            console.log(chalk_1.red(`stderr: ${stderr}`));
        }
        // console.log(stdout);
        resolve(stdout);
    });
}));
exports.commandLine = commandLine;
