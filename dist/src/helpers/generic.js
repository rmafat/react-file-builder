"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addTrailingSlash = exports.objectHasStructure = exports.initCap = void 0;
/**
 * Uppercase the first letter of word and, let the rest to lowercase.
 *
 * @param word
 */
const initCap = (word) => word.charAt(0).toUpperCase() + word.slice(1);
exports.initCap = initCap;
/**
 * Check if obj has all given props has key.
 *
 * @param obj
 * @param props
 */
const objectHasStructure = (obj, props) => (props.find((prop) => obj[prop] === undefined) === undefined);
exports.objectHasStructure = objectHasStructure;
/**
 * Add a trailing slash to strings that don't have one.
 *
 * @param value
 */
const addTrailingSlash = (value) => {
    const lastCharacter = value.substr(-1);
    return '/' === lastCharacter ? value : `${value}/`;
};
exports.addTrailingSlash = addTrailingSlash;
