"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatTemplateParam = void 0;
/**
 * Transform an object on string to be usable as Hygen template params.
 *
 * @param params
 */
const formatTemplateParam = (params) => (Object.keys(params).reduce((acc, paramKey) => (
// @ts-ignore
`${acc} --${paramKey} ${params[paramKey]}`), ''));
exports.formatTemplateParam = formatTemplateParam;
