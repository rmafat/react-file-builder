"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logDirectoryFiles = exports.parseJsonFile = exports.fromCwd = exports.fromRfbDir = void 0;
const tslib_1 = require("tslib");
const path = tslib_1.__importStar(require("path"));
const fs_1 = tslib_1.__importDefault(require("fs"));
/**
 * Get the absolute path of an element, from the CLI directory.
 *
 * @param filePath
 *   The path we want from the CLI directory.
 */
const fromRfbDir = (filePath) => {
    if (!require.main) {
        throw new Error('An error occurred while creating Concerto structure.');
    }
    const distDir = path.dirname(require.main.filename);
    const distDirSplitted = distDir.split('/');
    // Remove dist directory.
    distDirSplitted.pop();
    return `${distDirSplitted.join('/')}${filePath}`;
};
exports.fromRfbDir = fromRfbDir;
/**
 * Get the current working directory.
 *
 * @param filePath
 */
const fromCwd = (filePath) => {
    const formattedFilePath = filePath.substr(0, 1) === '/'
        ? filePath.substr(1)
        : filePath;
    return `${process.cwd()}/${formattedFilePath}`;
};
exports.fromCwd = fromCwd;
/**
 * Parse Json object from a given Json file.
 *
 * @param filePath
 */
const parseJsonFile = (filePath) => {
    if (fs_1.default.existsSync(filePath)) {
        return JSON.parse(fs_1.default.readFileSync(filePath, 'utf8'));
    }
    throw new Error(`The file ${filePath} doesn't exists.`);
};
exports.parseJsonFile = parseJsonFile;
/**
 * List files inside a directory.
 *
 * @param directory
 */
const logDirectoryFiles = (directory) => {
    if (!fs_1.default.existsSync(directory)) {
        throw new Error(`The directory ${directory} doesn't exists.`);
    }
    const files = fs_1.default.readdirSync(directory, {
        encoding: 'utf8',
        withFileTypes: true,
    });
    files.map((file) => console.log(`* ${directory}/${file.name}`));
};
exports.logDirectoryFiles = logDirectoryFiles;
