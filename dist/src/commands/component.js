"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateComponent = void 0;
const chalk_1 = require("chalk");
const generic_1 = require("../helpers/generic");
const preferencesHandler_1 = require("../handlers/preferencesHandler");
const fileGenerator_1 = require("../handlers/fileGenerator");
const fileChecker_1 = require("../handlers/fileChecker");
const Preferences_1 = require("../models/Preferences");
/**
 * Command controller to generate React components.
 *
 * @param name
 * @param options
 */
const generateComponent = async (name, options) => {
    try {
        const preferences = preferencesHandler_1.readPreferences();
        const componentName = generic_1.initCap(name);
        if (options.dir) {
            preferences[Preferences_1.PreferencesKeys.DIRECTORY] = generic_1.addTrailingSlash(options.dir);
        }
        await fileChecker_1.checkForExistingComponent(preferences[Preferences_1.PreferencesKeys.DIRECTORY] + componentName);
        await fileGenerator_1.generateComponentFiles(componentName, preferences);
    }
    catch (e) {
        console.log(chalk_1.red(e));
    }
};
exports.generateComponent = generateComponent;
