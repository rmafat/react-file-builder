"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.init = void 0;
const tslib_1 = require("tslib");
const inquirer_1 = tslib_1.__importDefault(require("inquirer"));
const chalk_1 = require("chalk");
const Preferences_1 = require("../models/Preferences");
const fileGenerator_1 = require("../handlers/fileGenerator");
const preferences_1 = require("../config/preferences");
/**
 * Prompt to ask user preferences.
 */
const askForPreferences = () => (inquirer_1.default.prompt([{
        type: 'confirm',
        name: Preferences_1.PreferencesKeys.REACT_NATIVE,
        message: 'Is it a React Native project ?',
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.REACT_NATIVE],
    }, {
        type: 'confirm',
        name: Preferences_1.PreferencesKeys.TYPESCRIPT,
        message: 'Do you use Typescript on your project ?',
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.TYPESCRIPT],
    }, {
        type: 'confirm',
        name: Preferences_1.PreferencesKeys.TESTS,
        message: 'Do you want RFB command to generate tests files for each component ?',
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.TESTS],
    }, {
        type: 'confirm',
        name: Preferences_1.PreferencesKeys.STYLES,
        message: 'Do you want RFB command to generate styles files for each components ?',
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.STYLES],
    }, {
        type: 'list',
        name: Preferences_1.PreferencesKeys.STYLES_EXTENSION,
        message: 'What styles extension do you use ?',
        choices: Object.values(Preferences_1.StylesExtensions),
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.STYLES_EXTENSION],
        when: (answers) => answers.generateStyles,
    }, {
        type: 'input',
        name: Preferences_1.PreferencesKeys.DIRECTORY,
        message: 'Where do you want to generate your components ?',
        default: preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.DIRECTORY],
    }]));
/**
 * Command controller to initialize RFB by defining user preferences.
 */
const init = async () => {
    const userPreferences = await askForPreferences();
    // To be sure preferences are complete, for example if styles extensions are skipped.
    const completePreferences = {
        ...preferences_1.preferencesDefaultValues,
        ...userPreferences,
    };
    fileGenerator_1.generateRfbPreferencesFile(completePreferences);
    console.log(chalk_1.green('\n.rfb file generated successfully !'));
};
exports.init = init;
