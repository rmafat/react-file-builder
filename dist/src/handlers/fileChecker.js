"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkForExistingComponent = void 0;
const tslib_1 = require("tslib");
const fs_1 = tslib_1.__importDefault(require("fs"));
const inquirer_1 = tslib_1.__importDefault(require("inquirer"));
const chalk_1 = require("chalk");
/**
 * Ask the user for override the existing component.
 */
const askForOverride = () => (inquirer_1.default.prompt([{
        type: 'confirm',
        name: 'override',
        message: 'Do you want to override the component and reset it ?',
        default: false,
    }]));
/**
 * Check if the component already exists and warn the user.
 *
 * @param componentPath
 */
const checkForExistingComponent = async (componentPath) => {
    if (fs_1.default.existsSync(componentPath)) {
        console.log(chalk_1.yellow(`Warning : It seems that the ${componentPath} component already exists.`));
        const question = await askForOverride();
        if (!question.override) {
            console.log(chalk_1.red('Command stopped, the component is not created.'));
            process.exit();
        }
    }
};
exports.checkForExistingComponent = checkForExistingComponent;
