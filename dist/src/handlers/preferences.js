"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readPreferences = void 0;
const Preferences_1 = require("../models/Preferences");
const fileSystem_1 = require("../helpers/fileSystem");
const generic_1 = require("../helpers/generic");
/**
 * Read preferences from .rgb user file.
 */
const readPreferences = () => {
    const userPreferences = fileSystem_1.parseJsonFile(fileSystem_1.fromCwd('.rfb'));
    if (!generic_1.objectHasStructure(userPreferences, Object.values(Preferences_1.PreferencesKeys))) {
        throw new Error('.rfb file hasn\'t the good structure. Please run `rfb init` to regenerate it.');
    }
    return userPreferences;
};
exports.readPreferences = readPreferences;
