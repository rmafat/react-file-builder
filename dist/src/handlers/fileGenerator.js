"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateComponentFiles = exports.generateRfbPreferencesFile = void 0;
const tslib_1 = require("tslib");
const fs_1 = tslib_1.__importDefault(require("fs"));
const chalk_1 = require("chalk");
const Preferences_1 = require("../models/Preferences");
const fileSystem_1 = require("../helpers/fileSystem");
const cli_1 = require("../helpers/cli");
const fileGenaration_1 = require("../helpers/fileGenaration");
const preferences_1 = require("../config/preferences");
/**
 * Write user preferences into .rfb conf file.
 */
const generateRfbPreferencesFile = (preferences) => {
    fs_1.default.writeFile(preferences_1.RFB_CONFIG_FILE, JSON.stringify(preferences, null, 2), (error) => error && console.error(chalk_1.red(error)));
};
exports.generateRfbPreferencesFile = generateRfbPreferencesFile;
/**
 * Generate a template using Hygen command.
 *
 * @param templateName
 *   The template name to generate.
 * @param params
 *   All params passed to template.
 */
const generateTemplate = async (templateName, params) => {
    const templateDir = fileSystem_1.fromRfbDir('/_templates');
    const environment = `HYGEN_TMPLS=${templateDir} HYGEN_OVERWRITE=1`;
    const command = `${environment} hygen rfb ${templateName} ${fileGenaration_1.formatTemplateParam(params)}`;
    await cli_1.commandLine(command);
};
/**
 * Log list of files generated on the user console.
 *
 * @param params
 */
const logCreatedFiles = (params) => {
    console.log(chalk_1.green('Created files : \n'));
    fileSystem_1.logDirectoryFiles(params[Preferences_1.PreferencesKeys.DIRECTORY] + params.componentName);
};
/**
 * Generate all components files, depending on user preferences.
 *
 * @param componentName
 * @param preferences
 */
const generateComponentFiles = async (componentName, preferences) => {
    const params = {
        ...preferences,
        componentName,
    };
    await generateTemplate(preferences[Preferences_1.PreferencesKeys.TYPESCRIPT] ? 'TSXComponent' : 'JSXComponent', params);
    if (preferences[Preferences_1.PreferencesKeys.STYLES]) {
        await generateTemplate('styles', params);
    }
    if (preferences[Preferences_1.PreferencesKeys.TESTS]) {
        await generateTemplate('tests', params);
    }
    logCreatedFiles(params);
};
exports.generateComponentFiles = generateComponentFiles;
