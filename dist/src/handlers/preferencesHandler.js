"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readPreferences = void 0;
const chalk_1 = require("chalk");
const Preferences_1 = require("../models/Preferences");
const fileSystem_1 = require("../helpers/fileSystem");
const generic_1 = require("../helpers/generic");
const preferences_1 = require("../config/preferences");
/**
 * Apply some check to user preferences.
 *
 * @param preferences
 */
const checkPreferences = (preferences) => {
    const checkedPreferences = preferences;
    if (!Object.values(Preferences_1.StylesExtensions).includes(preferences[Preferences_1.PreferencesKeys.STYLES_EXTENSION])) {
        console.log(chalk_1.yellow(`
      .rfb file hasn\'t valid values for '${Preferences_1.PreferencesKeys.STYLES_EXTENSION}' key.
      Allowed values are : ${Object.values(Preferences_1.StylesExtensions).join(', ')}.
      Default value '${preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.STYLES_EXTENSION]}' will be used.
    `));
        checkedPreferences[Preferences_1.PreferencesKeys.STYLES_EXTENSION] = preferences_1.preferencesDefaultValues[Preferences_1.PreferencesKeys.STYLES_EXTENSION];
    }
    if (!generic_1.objectHasStructure(preferences, Object.values(Preferences_1.PreferencesKeys))) {
        console.log(chalk_1.yellow(`
      .rfb file hasn\'t the complete required structure.
      Default values will be used.
      Please run 'rfb init' to regenerate it.
    `));
    }
    return checkedPreferences;
};
/**
 * Format some user preferences values to ensure validity.
 *
 * @param preferences
 */
const formatPreferences = (preferences) => {
    const formattedPreferences = preferences;
    if (preferences[Preferences_1.PreferencesKeys.DIRECTORY]) {
        formattedPreferences[Preferences_1.PreferencesKeys.DIRECTORY] = generic_1.addTrailingSlash(preferences[Preferences_1.PreferencesKeys.DIRECTORY]);
    }
    return formattedPreferences;
};
/**
 * Read preferences from .rgb user file.
 */
const readPreferences = () => {
    let userPreferences = fileSystem_1.parseJsonFile(fileSystem_1.fromCwd(preferences_1.RFB_CONFIG_FILE));
    userPreferences = formatPreferences(userPreferences);
    userPreferences = checkPreferences(userPreferences);
    return {
        ...preferences_1.preferencesDefaultValues,
        ...userPreferences,
    };
};
exports.readPreferences = readPreferences;
