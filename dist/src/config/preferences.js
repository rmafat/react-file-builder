"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.preferencesDefaultValues = exports.RFB_CONFIG_FILE = void 0;
const Preferences_1 = require("../models/Preferences");
exports.RFB_CONFIG_FILE = '.rfb.json';
exports.preferencesDefaultValues = {
    [Preferences_1.PreferencesKeys.REACT_NATIVE]: false,
    [Preferences_1.PreferencesKeys.TYPESCRIPT]: false,
    [Preferences_1.PreferencesKeys.TESTS]: true,
    [Preferences_1.PreferencesKeys.STYLES]: true,
    [Preferences_1.PreferencesKeys.STYLES_EXTENSION]: Preferences_1.StylesExtensions.SCSS,
    [Preferences_1.PreferencesKeys.DIRECTORY]: 'src/components/',
};
