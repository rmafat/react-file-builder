# CHANGELOG

## v1.1.0

**DATE** : 2022-02-19

**FEATURES** :

- Add option to specify the directory where to generate the component
  - If the path doesn't exist, directories are created.
  - `rootComponentsDirectory` preference from `rfb.json` file is ignored.
  
## v1.0.5

**DATE** : 2022-02-19

**FIXES** :

- Fix indentation for return block on component template.
- Add missing slash on created files logs.

**FEATURES** :

- Better styles generation for React Native templates
  - Style file is generated with an empty `StyleSheet.create({})` exported by default.
  - Add `import styles from` on the component file, to easily use styles.
 
## v1.0.4

**DATE** : 2021-03-04

**FIXES** :

- Fix template mistake made on 1.0.3.

## v1.0.3

**DATE** : 2021-03-03

**FIXES** :

- Change default export on index to have a prettier import, without brackets.
- Print prettier logs after files generation.
- Check if the component already exists and ask user before override.

## 1.0.2

**DATE** : 2021-01-07

**FIXES** :

- Rename `.rfb` file to `.rfb.json` for better syntax highlighting.
- Fix `index.ts` extension for Typescript projects.
- Remove _lowercase_ functionnality for component names formating.
- Improve code generated on `.rfb.json` by settings defaults values if necessary.

**FEATURES** :  

- Add new preference `isReactNative`
  - For now, it's mainly for style file problems.

## 1.0.1

**DATE** : 2021-01-04

**FIXES** :

- Complete documentation
- Complete `package.json` for NPM page

## 1.0.0 

**DATE** : 2021-01-03

**FEATURES** :

- First release
- `rfb init` command
- `rfb component <componentName>` command
