#!/usr/bin/env node

import program from 'commander';
import { fromRfbDir, parseJsonFile } from './src/helpers/fileSystem';
import { generateComponent } from './src/commands/component';
import { init } from './src/commands/init';
import { Options } from './src/models/Command';

// Register the tsconfig allow CLI to read TS files.
const tsConfig = parseJsonFile(fromRfbDir('/tsconfig.json'));
require('ts-node').register(tsConfig);

program.version('1.0.0');

program.command('init')
  .description('Initialize and configure react-file-builder command.')
  .action(() => init());

program.command('component <componentName>')
  .description('Generate files for a JSXComponent JSXComponent.')
  .option('-d, --dir <directoryPath>', 'Directory path where to generate the component.')
  .action((componentName: string, options: Options) => generateComponent(componentName, { dir: options.dir }));

program.parse(process.argv);
