---
to: <%= rootComponentsDirectory %><%= componentName %>/<%= componentName %><%= isReactNative=='true'?`Styles`:`` %><%= stylesExtension %>
---
<% if (isReactNative == 'true') { %>import { StyleSheet } from 'react-native';

export default StyleSheet.create({

});
<% } %>
