---
to: <%= rootComponentsDirectory %><%= componentName %>/index.js
---
import <%= componentName %> from './<%= componentName %>';

export default <%= componentName %>;
