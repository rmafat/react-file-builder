---
to: <%= rootComponentsDirectory %><%= componentName %>/<%= componentName %>.jsx
---
import React from 'react';
<% if (generateStyles == 'true') { %>import<%= isReactNative=='true'?` styles from `:` ` %>'./<%= componentName %><%= isReactNative=="true"?`Styles`:stylesExtension %>';<% } %>

const <%= componentName %> = (props) => {
  return (
    <>
    </>
  );
}

export default <%= componentName %>;
