---
to: <%= rootComponentsDirectory %><%= componentName %>/<%= componentName %>.tsx
---
import React from 'react';
<% if (generateStyles == 'true') { %>import<%= isReactNative=='true'?` styles from `:` ` %>'./<%= componentName %><%= isReactNative=="true"?`Styles`:stylesExtension %>';<% } %>

interface Props {}

const <%= componentName %>: React.FC<Props> = (props: Props) => {
  return (
    <>
    </>
  );
}

export default <%= componentName %>;
