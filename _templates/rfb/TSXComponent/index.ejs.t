---
to: <%= rootComponentsDirectory %><%= componentName %>/index.ts
---
import <%= componentName %> from './<%= componentName %>';

export default <%= componentName %>;
